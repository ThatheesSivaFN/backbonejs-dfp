var AppAboutView = Backbone.View.extend({
    id: "app",
    initialize: function() {
        this.render();
    },
    render: function() {
        this.$el.html('<div id="content"><div><h1>About</h1></div><div id="menu-bar"></div></div>');
        this.appMenuView = new AppMenuView();
        this.assign({
            "#menu-bar": this.appMenuView
        });
        return this;
    }
});

_.extend(AppAboutView.prototype, SubViewMixin);