var AppHomeView = Backbone.View.extend({
    id: "app",
    initialize: function() {
        this.render();
    },
    render: function() {
        this.$el.html('<div id="content"><div><h1>Home</h1></div><div id="menu-bar"></div><div id="one"></div><div id="two"></div></div>');
        this.appMenuView = new AppMenuView();
        this.demoAd = new DfpAdvertisementSlot.Demo();
        this.mobileAd = new DfpAdvertisementSlot.MobileHomePage();

        this.assign({
            "#menu-bar": this.appMenuView,
            "#one": this.demoAd,
            "#two": this.mobileAd
        });

        return this;
    }
});

_.extend(AppHomeView.prototype, SubViewMixin);