DfpAdvertisementSlot.MobileHomePage = DfpAdvertisementSlot.View.extend({
    model: new DfpAdvertisementSlot({
        adSlotWidth: 320,
        adSlotHeight: 50,
        adSlotSelectorId: "div-gpt-ad-1443801930909-0",
        adSlotClassName: "div-gpt-ad-1443801930909-0",
        adSlotName: "/71296542/BTF_Mobile_Leaderboard_320x50"
    })
});
