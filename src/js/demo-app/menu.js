var AppMenuView = Backbone.View.extend({
    render: function() {
        this.$el.html('<div class="menu"><a id="home" href="#">Home</a> | <a id="about" href="#">About</a></div>');
        return this;
    },
    events: {
        "click #home": "home",
        "click #about": "about"
    },
    home: function(event) {
        event.preventDefault();
        appRouter.navigate('home', {
            trigger: true
        });
    },
    about: function(event) {
        event.preventDefault();
        appRouter.navigate('about', {
            trigger: true
        });
    }
});