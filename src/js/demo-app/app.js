var AppRouter = Backbone.Router.extend({
    routes: {
        "home": "index",
        "about": "about"
    },
    initialize: function() {
        this.index();
    },

    index: function() {
        this.appHomeView = new AppHomeView();
        $("body").html(this.appHomeView.el);
    },

    about: function() {
        this.appAboutView = new AppAboutView();
        $("body").html(this.appAboutView.el);
    },

    start: function() {
        Backbone.history.start({
            pushState: false,
            hashChange: true
        });
    }
});

var appRouter;

$(function() {
    appRouter = new AppRouter();
    appRouter.start();
});