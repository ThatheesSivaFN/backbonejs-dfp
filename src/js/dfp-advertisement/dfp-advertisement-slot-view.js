DfpAdvertisementSlot.View = Backbone.View.extend({
    template: _.template(
        '<div class="<%= adSlotComponentName %>"><div id="<%= adSlotComponentName %>__<%= adSlotSelectorId %>" ' +
        'style="width:<%= adSlotWidth %><%= adSlotDimensionUnitType %>; height:<%= adSlotHeight %><%= adSlotDimensionUnitType %>;" ' +
        'class="<%= adSlotComponentName %>__<%= adSlotClassName %>">' +
        '<script>if(DfpAdvertisementSlot.View.isGooglePublisherTagDefined()) googletag.cmd.push(function() {googletag.display("<%= adSlotComponentName %>__<%= adSlotSelectorId %>");});</script>' +
        '</div></div>'
    ),
    initialize: function() {
        this.enableAdSlot();
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()))
        return this;
    },
    enableAdSlot: function() {
        if (DfpAdvertisementSlot.View.isGooglePublisherTagDefined()) {
            googletag.cmd.push(this.defineAdSlot());
        }
    },
    defineAdSlot: function() {
        var self = this;
        return function() {
            var adSlot = googletag.defineSlot(
                self.model.get("adSlotName"), [
                    self.model.get("adSlotWidth"),
                    self.model.get("adSlotHeight")
                ],
                self.createBemCssSelectorName(self.model.get("adSlotSelectorId"))
            );
            self.enableGoogleTagServices(adSlot);
        };
    },
    enableGoogleTagServices: function(adSlot) {
        adSlot.addService(googletag.pubads());
        if (this.model.get("hasEnabledSingleRequest")) {
            googletag.pubads().enableSingleRequest();
        }
        googletag.enableServices();
    },
    createBemCssSelectorName: function(selector) {
        return this.model.get("adSlotComponentName") + "__" + selector;
    }
});

DfpAdvertisementSlot.View.isGooglePublisherTagDefined = function() {
    return window.googletag !== undefined;
};