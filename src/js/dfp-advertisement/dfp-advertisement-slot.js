var DfpAdvertisementSlot = Backbone.Model.extend({
    defaults: {
        adSlotComponentName: "dfp-ad-banner",
        adSlotWidth: 300,
        adSlotHeight: 250,
        adSlotSelectorId: "default",
        adSlotClassName: "default",
        adSlotName: "/6355419/Travel/Europe/France/Paris",
        adSlotDimensionUnitType: "px",
        hasEnabledSingleRequest: true
    }
});